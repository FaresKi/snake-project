import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Board extends JPanel implements ActionListener {
    private final int B_WIDTH = 300;
    private final int B_HEIGHT = 300;
    private final int DOT_SIZE = 10;
    private final int ALL_DOTS = 900;
    private final int RANDOM_POS = 42;
    private final int DELAY = 120;

    private final int[] x = new int[ALL_DOTS];
    private final int[] y = new int[ALL_DOTS];

    private int dots_snake;
    private int apple_x;
    private int apple_y;

    private boolean leftDirection = false;
    private boolean rightDirection = false;
    private boolean upDirection = false;
    private boolean downDirection = false;

    private boolean inGame = true; //we suppose we're always in the game, unless we lose

    private Timer timer;
    private Image head;
    private Image apple;
    private Image body;

    private void initBoard(){

        addKeyListener(new Directions());
        setBackground(Color.BLACK);
        setPreferredSize(new Dimension(B_WIDTH,B_HEIGHT));
        loadImages();

    }

    private void loadImages(){
        ImageIcon i_head = new ImageIcon("src/Pictures/head.png");
        head = i_head.getImage();

        ImageIcon i_apple = new ImageIcon("src/Pictures/apple.png");
        apple = i_apple.getImage();

        ImageIcon i_body = new ImageIcon("src/Pictures/dot.png");
        body = i_body.getImage();
    }

    private void initGame(){
        dots_snake=3;
        for(int z=0;z<dots_snake;z++){
            x[z]=50 - z*10;
            y[z]=50;
        }
        findApple();
        timer = new Timer(DELAY,this);
        timer.start();

    }

    private void findApple(){
        int rand = (int) (Math.random() * RANDOM_POS);
        apple_x = rand;
        rand = (int) (Math.random() * RANDOM_POS);
        apple_y = rand;

    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
    }

    public void doDrawing(Graphics g){
        if(inGame){
            //fuck this we'll do it later
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }


    private class Directions extends KeyAdapter{

        @Override
        public void keyPressed(KeyEvent e) {
            int key_value = e.getKeyCode();
            if(key_value==KeyEvent.VK_UP && (!downDirection)){
                upDirection=true;
                leftDirection=false;
                rightDirection=false;
            }

            if(key_value==KeyEvent.VK_DOWN && (!upDirection)){
                downDirection=true;
                leftDirection=false;
                rightDirection=false;
            }

            if(key_value==KeyEvent.VK_LEFT && (!rightDirection)){
                leftDirection=true;
                upDirection=false;
                downDirection=false;
            }

            if(key_value==KeyEvent.VK_RIGHT && (!leftDirection)){
                rightDirection=true;
                upDirection=false;
                downDirection=false;
            }



        }
    }
}
